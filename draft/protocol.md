# PROTOCOL

* weekly from noon to 6pm
* anyone can join and leave any time
* takes place on irc because:
  - platform neutral
  - no auth required
  - no sensitive information
  - can run own server if we want, easy canonical log of session
  - still widely in use for hands-on tech support
  - no GDPR woes with uploaded media content because not supported
* all participants are encouraged to give structured evaluation of the software after session
  - see EVALUATION below
  - encouraged to digitally sign the evaluation, any signature scheme allowed
* how are the trial projects chosen?


## EVALUATION

* each criteria gets a rating
  - scale 0.0 to 1.0
* each criteria gets an optional (but encouraged) description 
  - up to 300 words? 
* given in a form that can generate canonical representation and then be signed
* average scores for the sessions are published


### CRITERIA

1. accessibility
  * how easy is the software/library to use, understand
  * quality, scope and actuality of the documentation
  * do errors and exceptions contain good information

2. integrity
  * quantity and quality of dependencies
  * what are the effects and side-effects of using the software, and potential consequences (third-party bindings etc)
  * are the rules of engagement and contribution well defined
  * how vital is the community (issues, PRs, forums, channels)

3. portability
  * how easy to get data in and out
  * completeness of data out, tolerance for custom data in
  * are data formats and structures sane and legible


## ROLES

* contributor: anyone joining any session, and may have evaluated the session
* signed contributor: contributor that evaluated a session and signed the evalutaion
* trusted contributor: signed contributor whose key has a record in trusted registry for the 
* editor: can publish reports to common name resource pointer

## SCALING

In case of volume of adoption, some thoughts:

* we can manually define certain public keys as trusted, to filter out spam
  - different score averages can be shown (like metacritic), trusted/non-trusted...
* governance protocols
  - vote on who are trusted, who are editors
  - feed-like individual graphs/views of trust and content
* parallell sessions with different software


