# REPORT

Outline of contents of report page, simple first version

The term "useful" and "usefulness" is the discretion of the editor(s) in the first incarnation of the project.

## SUMMARY

* name of project tested
* date
* author(s) of summary
* text

## RESOURCES

* useful links discovered in sessions (stackoverflow etc)
* keywords
* links to useful example code/script/instructions created by session contributors 

## SCORE

* score value for each criteria

## EVALUATIONS

* each contributor's score and written evaluation
* ordered by usefulness 

## SESSION LOG

* initially showing only "useful highlights,"
* can toggle "full chat log"

## SIGNATURE

* signable by anyone else who want to certify the contents as correct
* may be solved by forking and git hashes also/instead
